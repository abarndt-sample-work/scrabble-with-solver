/**
 * @author: Andrew cannot claim credit for this, as a side project of mine
 * over break involved solving a word search and I searched trie
 * implementations in Java and found
 * https://www.programcreek.com/2014/05/leetcode-implement-trie-prefix-tree
 * -java/
 * Since I already had a fully-functioning trie that I used over winter break,
 * I thought it best to just reuse it. This class is the trie from that page,
 * adapted for use in this project in a couple major ways: 1) the TrieNode
 * class is in a separate .java file, 2) the startsWith method provided in that
 * file is unused in this project and has been removed, 3) the code is spaced
 * more readably and appropriately, and 4) the root node is made private. Most
 * importantly, I know that I understand how this code works.
 */

package scrabble;

public class Trie {
    private TrieNode root;

    /**
     * Constructs the trie with just one initial node to start. More are added
     * as words are added.
     */
    public Trie() {
        root = new TrieNode();
    }

    /**
     * Inserts a word into the trie by starting at the root and working down
     * the nodes within. If a node where that letter would be is null, a new
     * node is created. When the word is finished being added, the last note
     * visited is marked as an ending node.
     * @param word the word being added to the trie.
     */
    public void insert(String word) {
        TrieNode p = root;
        for (int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            int index = c - 'a';
            if (p.getArr()[index] == null){
                TrieNode temp = new TrieNode();
                p.getArr()[index] = temp;
                p = temp;
            } else {
                p = p.getArr()[index];
            }
        }
        p.setEnd(true);
    }

    // Returns if the word is in the trie.

    /**
     * Searches to see if a word is within the trie. If a word isn't in the
     * trie, it's not a valid Scrabble play.
     * @param word the word being searched for.
     * @return true if the word is in the trie; false if not
     */
    public boolean search(String word) {
        TrieNode p = searchNode(word);
        if(p == null) return false;
        else if (p.isEnd()) return true;
        return false;
    }

    /**
     * Searches the trie, looking for a given string. Traverses one level
     * deeper for each character until either the end of the string is reached
     * or a null node is encountered.
     * @param s the word being searched for.
     * @return the node the search for the word stops at. Null if the string
     * is not within the trie, and a regular trie node if the string is within
     * the trie.
     */
    public TrieNode searchNode(String s) {
        TrieNode p = root;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int index = c - 'a';
            if (p.getArr()[index] != null){
                p = p.getArr()[index];
            } else return null;
        }
        if (p == root) return null;
        return p;
    }

    /**
     *
     * @return
     */
    public TrieNode getRoot() {
        return root;
    }
}
