/**
 * @author: Andrew Barndt
 * Class description: a class that finds the highest-scoring Scrabble word,
 * given a board and a tray of Scrabble tiles.
 */

package scrabble;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class Solver {

    private Trie trie;
    private Set<Pair<Integer, Integer>> anchorSquares = new HashSet<>();
    private BestWord highScore = new BestWord(-1, -1, 0, "", true);
    private Board b;
    private Square origin;

    /**
     * Solver constructor. Creates a trie of all lowercase letters.
     * @param s the dictionary file name to be used for the solver.
     * @param comLine if the file was provided by the command line. If it was,
     *                the program looks within the current directory for the
     *                file. If it wasn't, it looks within the Resources folder.
     */
    public Solver(String s, boolean comLine) {
        trie = new Trie();
        InputStream in;
        if (comLine) in = getClass().getResourceAsStream(s);
        else in = getClass().getClassLoader().getResourceAsStream(s);
        Scanner scanner =
                new Scanner(new BufferedReader(new InputStreamReader(in)));
        while (scanner.hasNextLine()) {
            String str = scanner.nextLine();
            trie.insert(str.toLowerCase());
        }
    }

    /**
     * Helper method for determining if a point is an anchor square by checking
     * if the tile is adjacent to an already-placed tile, while also not being
     * a square with a tile placed on it already.
     * @param sq the square being checked for adjacency.
     * @return true if the square is adjacent to a placed tile, and false if it
     * isn't.
     */
    private boolean isAdjacent(Square sq) {
        boolean nextTo = false;
        if (!sq.isPlayed()) {
            if (b.getBoard()[sq.getY() + 1][sq.getX()].isPlayed()
                    || b.getBoard()[sq.getY() - 1][sq.getX()].isPlayed()
                    || b.getBoard()[sq.getY()][sq.getX() + 1].isPlayed()
                    || b.getBoard()[sq.getY()][sq.getX() - 1].isPlayed()) {
                nextTo = true;
            }
        }
        return nextTo;
    }

    /**
     * Goes through the entire board and checks for adjacency. If a square is
     * empty and has an adjacent played tile (as checked by the isAdjacent
     * method), then its coordinates are placed into a list of x-y Pairs of
     * all anchor tiles.
     */
    private void makeAnchorSquares() {
        for (int i = 1; i < b.getDimension() + 1; i++) {
            for (int j = 1; j < b.getDimension() + 1; j++) {
                if (isAdjacent(b.getBoard()[i][j])) {
                    anchorSquares.add(new Pair<>(j, i));
                    b.getBoard()[i][j].setAnchorSquare(true);
                }
            }
        }
    }

    /**
     * Calculates the points as described in the Checker class. If the score
     * is higher than the previously calculated best word, the previous best
     * word is replaced with the new best word.
     * @param word the word being scored.
     * @param x the x coordinate of the word's start.
     * @param y the y coordinate of the word's start.
     * @param toRight if the word goes to the right or not.
     */
    public void calculatePoints(String word, int x, int y, boolean toRight) {
        int pointTotal = Checker.calculatePoints(b, word, x, y, toRight);
        if (highScore.getPoints() < pointTotal) {
            highScore = new BestWord(x, y, pointTotal, word, toRight);
        }
    }

    /**
     * The first half of the solver algorithm. Takes a board and a string
     * representing the tray. Starts with a default best word, and if no
     * valid word is found, the method returns false. Starts by refreshing the
     * list of anchor squares (or if none are found, the only anchor tile is
     * the middle tile, as it must be the start of the game). Then we find all
     * possible viable starting phrases to the left of the anchor tile (up to
     * either the end of the board, 6 letters to the left, or until hitting
     * an already-played tile), and send it to the leftPart method. That
     * process is repeated then, except for vertical words.
     * @param board the current board.
     * @param tray the tray of letters a player has.
     * @return true if there's a word with more than 0 points; false otherwise.
     */
    public boolean solve(Board board, String tray) {
        b = board;
        highScore = new BestWord(-1, -1, 0, "", true);
        anchorSquares.clear();
        makeAnchorSquares();
        if (anchorSquares.size() == 0) {
            anchorSquares.add(new Pair<>(b.getDimension()/2 + 1,
                    b.getDimension()/2 + 1));
        }
        boolean hasMove = true;
        List<Character> rack = new LinkedList<>();
        for (int i = 0; i < tray.length(); i++) {
            rack.add(tray.charAt(i));
        }
        for (Pair<Integer, Integer> pair: anchorSquares) {
            origin = b.getBoard()[pair.getValue()][pair.getKey()];
            int horLimit = 0;
            int x = pair.getKey();
            int y = pair.getValue();
            String s = "";
            if (x > 1 && !b.getBoard()[y][--x].isAnchorSquare()) {
                if (b.getBoard()[y][x].isPlayed()) {
                    s = b.getBoard()[y][x].getC() + s;
                    while (b.getBoard()[y][--x].isPlayed()) {
                        s = b.getBoard()[y][x].getC() + s;
                    }
                } else {
                    horLimit++;
                    while (x > 1 && !b.getBoard()[y][--x].isAnchorSquare()
                            && horLimit < rack.size() - 1) {
                        horLimit++;
                    }
                }
            }
            TrieNode node;
            if (s.length() > 0) {
                node = trie.searchNode(s.toLowerCase());
            }
            else node = trie.getRoot();
            if (s.length() < pair.getKey()) {
                leftPart(s, node, horLimit,
                        b.getBoard()[pair.getValue()][pair.getKey()], rack);
            }
        }
        for (Pair<Integer, Integer> pair: anchorSquares) {
            origin = b.getBoard()[pair.getValue()][pair.getKey()];
            int vertLimit = 0;
            int x = pair.getKey();
            int y = pair.getValue();
            String s = "";
            if (y > 1 && !b.getBoard()[--y][x].isAnchorSquare()) {
                if (b.getBoard()[y][x].isPlayed()) {
                    s = b.getBoard()[y][x].getC() + s;
                    while (b.getBoard()[--y][x].isPlayed()) {
                        s = b.getBoard()[y][x].getC() + s;
                    }
                } else {
                    vertLimit++;
                    while (y > 1 && !b.getBoard()[--y][x].isAnchorSquare()
                            && vertLimit < rack.size() - 1) {
                        vertLimit++;
                    }
                }
            }
            TrieNode node;
            if (s.length() > 0) {
                node = trie.searchNode(s.toLowerCase());
            }
            else node = trie.getRoot();
            if (s.length() < pair.getValue()) {
                topPart(s, node, vertLimit,
                        b.getBoard()[pair.getValue()][pair.getKey()], rack);
            }
        }
        if (highScore.getxStart() < 0) hasMove = false;
        return hasMove;
    }

    /**
     * The "leftPart" part of the algorithm described in the paper. Keeps
     * adding letters to the left of the anchor square, and sending them
     * to the extendRight method.
     * @param partialWord the
     * @param n the node within the trie the word is in. If the string is
     *          empty, then the node is the root; otherwise, the node is
     *          within the trie.
     * @param limit the remaining number of letters that can be added to the
     *              left side.
     * @param anchorSquare the anchor square acting as the basis for the word.
     * @param rack the player's tray; using a list makes it much easier to add
     *             and remove than using a string like was originally passed in
     *             to the solver.
     */
    private void leftPart(String partialWord, TrieNode n, int limit,
                          Square anchorSquare, List<Character> rack) {
        extendRight(partialWord, rack, n, anchorSquare);
        if (limit > 0) {
            for (int i = 0; i < n.getArr().length; i++) {
                if (n.getArr()[i] != null) {
                    //capitalize the letter, send it down a node
                    //for a blank: since we're already within a for loop, there's no
                    //need to make another loop to go through the array of nodes - it's
                    //already doing that.
                    if (rack.contains('*')) {
                        rack.remove((Character)'*');
                        String s = partialWord + (char)('A' + i);
                        leftPart(s, n.getArr()[i], limit - 1, anchorSquare, rack);
                        rack.add('*');
                    }
                    if (rack.contains((char)('a' + i))) {
                        rack.remove((Character)((char)('a' + i)));
                        String s = partialWord + "" + (char)('a' + i);
                        leftPart(s, n.getArr()[i], limit - 1, anchorSquare, rack);
                        rack.add((char)('a' + i));
                    }
                }
            }
        }
    }

    /**
     * The "extendRight" part of the paper's algorithm. Checks first if the
     * current square is occupied, if it isn't, check if the word is valid and
     * that we're not still at the anchor square and calculate the word's
     * points. After that, adds a letter at a time to the right, assuming the
     * letter works given the cross-checking. If the current square is occupied
     * then add that character, capitalized or not, to the word and recursively
     * call the method.
     * @param partialWord the word created so far; can be an actual valid word
     *                    or not.
     * @param rack the rack of characters.
     * @param n the current node within the trie.
     * @param sq the current square.
     */
    private void extendRight(String partialWord, List<Character> rack, TrieNode n, Square sq) {
        if (!sq.isPlayed()) {
            if (trie.search(partialWord.toLowerCase()) && !origin.equals(sq)) {
                calculatePoints(partialWord, sq.getX() - partialWord.length(),
                        sq.getY(), true);
            }
            for (int i = 0; i < n.getArr().length; i++) {
                if (n.getArr()[i] != null) {
                    if (!inCrossCheck((char)('a' + i), sq.getX(), sq.getY(),
                            true)) {
                        continue;
                    }
                    if (rack.contains('*')) {
                        rack.remove((Character)'*');
                        if (sq.getX() < b.getDimension() + 1) {
                            extendRight(partialWord + (char)('A' + i), rack,
                                    n.getArr()[i],
                                    b.getBoard()[sq.getY()][sq.getX() + 1]);
                        }
                        rack.add('*');
                    }
                    if (rack.contains((char)('a' + i))) {
                        rack.remove((Character)((char)('a' + i)));
                        if (sq.getX() < b.getDimension() + 1) {
                            extendRight(partialWord + (char)('a' + i), rack,
                                    n.getArr()[i],
                                    b.getBoard()[sq.getY()][sq.getX() + 1]);
                        }
                        rack.add((char)('a' + i));
                    }
                }
            }
        } else {
            char c = sq.getC();
            char trieTest = c;
            if (trieTest < 'a') trieTest += ' ';
            if (c != '0' && n.getArr()[trieTest - 'a'] != null) {
                extendRight(partialWord + c, rack, n.getArr()[trieTest - 'a'],
                        b.getBoard()[sq.getY()][sq.getX() + 1]);
            }
        }
    }

    /**
     * Same process as leftPart, except for y coordinates instead of x.
     * @param partialWord the
     * @param n the node within the trie the word is in. If the string is
     *          empty, then the node is the root; otherwise, the node is
     *          within the trie.
     * @param limit the remaining number of letters that can be added to the
     *              top.
     * @param anchorSquare the anchor square acting as the basis for the word.
     * @param rack the player's tray; using a list makes it much easier to add
     *             and remove than using a string like was originally passed in
     *             to the solver.
     */
    private void topPart(String partialWord, TrieNode n, int limit,
                          Square anchorSquare, List<Character> rack) {
        extendDown(partialWord, rack, n, anchorSquare);
        if (limit > 0) {
            for (int i = 0; i < n.getArr().length; i++) {
                if (n.getArr()[i] != null) {
                    //capitalize the letter, send it down a node
                    //for a blank: since we're already within a for loop, there's no
                    //need to make another loop to go through the array of nodes - it's
                    //already doing that.
                    if (rack.contains('*')) {
                        rack.remove((Character)'*');
                        String s = partialWord + (char)('A' + i);
                        topPart(s, n.getArr()[i], limit - 1, anchorSquare, rack);
                        rack.add('*');
                    }
                    if (rack.contains((char)('a' + i))) {
                        rack.remove((Character)((char)('a' + i)));
                        String s = partialWord + "" + (char)('a' + i);
                        topPart(s, n.getArr()[i], limit - 1, anchorSquare, rack);
                        rack.add((char)('a' + i));
                    }
                }
            }
        }
    }

    /**
     * Same process as extendRight, except for y coordinates instead of x.
     * @param partialWord the word created so far; can be an actual valid word
     *                    or not.
     * @param rack the rack of characters.
     * @param n the current node within the trie.
     * @param sq the current square.
     */
    private void extendDown(String partialWord, List<Character> rack, TrieNode n, Square sq) {
        if (!sq.isPlayed()) {
            if (trie.search(partialWord.toLowerCase()) && !origin.equals(sq)) {
                calculatePoints(partialWord, sq.getX(), sq.getY() - partialWord.length(), false);
            }
            for (int i = 0; i < n.getArr().length; i++) {
                if (n.getArr()[i] != null) {
                    if (!inCrossCheck((char)('a' + i), sq.getX(), sq.getY(), false)) {
                        continue;
                    }
                    if (rack.contains('*')) {
                        rack.remove((Character)'*');
                        if (sq.getY() < b.getDimension() + 1) {
                            extendDown(partialWord + (char) ('A' + i), rack,
                                    n.getArr()[i], b.getBoard()[sq.getY() + 1][sq.getX()]);
                        }
                        rack.add('*');
                    }
                    if (rack.contains((char)('a' + i))) {
                        rack.remove((Character)((char)('a' + i)));
                        if (sq.getY() < b.getDimension() + 1) {
                            extendDown(partialWord + (char) ('a' + i), rack,
                                    n.getArr()[i], b.getBoard()[sq.getY() + 1][sq.getX()]);
                        }
                        rack.add((char)('a' + i));
                    }
                }
            }
        } else {
            char c = sq.getC();
            char trieTest = c;
            if (trieTest < 'a') trieTest += ' ';
            if (c != '0' && n.getArr()[trieTest - 'a'] != null) {
                extendDown(partialWord + c, rack, n.getArr()[trieTest - 'a'],
                        b.getBoard()[sq.getY() + 1][sq.getX()]);
            }
        }
    }

    /**
     * Checks if a character, if placed at a given coordinate, would fit with
     * the rest of the tiles placed on the word.
     * @param c the character being added.
     * @param x the x-coordinate the letter is being added to.
     * @param y the y-coordinate the letter is being added to.
     * @param isRight if the word is played right or not.
     * @return true if the character can indeed be placed there; false if not.
     */
    private boolean inCrossCheck(char c, int x, int y, boolean isRight) {
        boolean valid = true;
        String s = "";
        if (isRight) {
            s += Checker.acrossCheck(b, c, x, y);
        } else {
            s += Checker.verticalCheck(b, c, x, y);
        }
        if (s.length() > 1) {
            if (!trie.search(s.toLowerCase())) valid = false;
        }
        return valid;
    }

    /**
     * Getter method for the highest scoring word.
     * @return the highest scoring word.
     */
    public BestWord getHighScore() {
        return highScore;
    }

    /**
     * Getter method for the trie the solver uses.
     * @return the trie.
     */
    public Trie getTrie() {
        return trie;
    }
}