/**
 * @author: Andrew Barndt
 * Class description: maintains a best word for the solver, to make sure the
 * best play is saved and can be played.
 */
package scrabble;

public class BestWord {

    private int xStart, yStart;
    private String word;
    private int points;
    private boolean isRight;

    /**
     * Constructor for the best word.
     * @param x the x-coordinate start of the word
     * @param y the y-coordinate start of the word
     * @param points the number of points the move scores
     * @param word the string (including both already-played tiles and tiles
     *             placed by the computer) of the word
     * @param horizontal if the word is played horizontal; false if the play
     *                   is vertical.
     */
    public BestWord(int x, int y, int points, String word, boolean horizontal) {
        xStart = x;
        yStart = y;
        this.word = word;
        this.points = points;
        isRight = horizontal;
    }

    /**
     * Getter method for the score.
     * @return points of the highest-scoring move.
     */
    public int getPoints() {
        return points;
    }

    /**
     * Getter method for the x-position the word starts at.
     * @return the x-coordinate of the beginning of the word.
     */
    public int getxStart() {
        return xStart;
    }

    /**
     * Getter method for the y-position the word starts at.
     * @return the y-coordinate of the beginning of the word.
     */
    public int getyStart() {
        return yStart;
    }

    /**
     * Getter method for the boolean indicating whether the word is played
     * horizontally or vertically.
     * @return true if the word is horizontal; false if vertical.
     */
    public boolean isRight() {
        return isRight;
    }

    /**
     * Getter method for the word being played, represented as a string.
     * @return the full word of the play.
     */
    public String getWord() {
        return word;
    }
}