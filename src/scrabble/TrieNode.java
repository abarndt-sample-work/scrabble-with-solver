/**
 * @author: same as the Trie.java class. Code adapted from
 * https://www.programcreek.com/2014/05/leetcode-implement-trie-prefix-tree
 * -java/
 * Major difference from what's provided in that link: the arr and isEnd fields
 * are made private here to follow the principle of least knowledge, which also
 * results in needing extra getters and setters.
 */
package scrabble;

public class TrieNode {
    private TrieNode[] arr;
    private boolean isEnd;

    //Initializes the trie structure

    /**
     * TrieNode constructor. Creates a node with space for 26 sub-nodes, in
     * accordance with there being 26 letters in the alphabet.
     */
    public TrieNode() {
        arr = new TrieNode[26];
    }

    /**
     * Getter method for this node.
     * @return the array of sub-nodes within.
     */
    public TrieNode[] getArr() {
        return arr;
    }

    /**
     * Getter method for the isEnd field.
     * @return true if this node marks the end of a word; false if it doesn't.
     */
    public boolean isEnd() {
        return isEnd;
    }

    /**
     * Setter method for setting the node as an ending node or not.
     * @param end true if the node is the end of a word; false if not.
     */
    public void setEnd(boolean end) {
        isEnd = end;
    }
}