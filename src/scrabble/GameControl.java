/**
 * @author: Andrew Barndt
 * Class description: The main game logic behind the game of Scrabble. Performs
 * much of the "behind the scenes" work of creating and keeping up the board,
 * score, whose turn it is, etc.
 */

package scrabble;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class GameControl {

    private Board b;
    private Solver sol;
    private Player human;
    private Player computer;
    private Player currentPlayer;
    private Bag bag;
    private List<Pair<Integer, Integer>> placed = new ArrayList<>();
    private int totalMoves = 0;
    private int moveScore = 0;

    /**
     * Constructor for a regular game of Scrabble. Starts with a blank board,
     * read line by line with a Scanner, then sets up the solver, bag, each
     * player, and determines the player who goes first.
     */
    public GameControl() {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream
                ("scrabble_board.txt");
             Scanner scanner = new Scanner(new BufferedReader
                     (new InputStreamReader(in)))) {
            while (scanner.hasNext()) {
                String num = scanner.nextLine();
                int size = Integer.parseInt(num);
                b = new Board(size);
                for (int i = 0; i < size; i++) {
                    b.rowOfBoard(scanner.nextLine(), i + 1);
                }
            }
        } catch (IOException ie) {
            System.out.println("Error loading the board.");
        }
        sol = new Solver("sowpods.txt", false);
        human = new Player("human");
        computer = new Player("computer");
        bag = new Bag();
        human.refreshTiles(bag);
        computer.refreshTiles(bag);
        if (Math.random() * 2 > 1) currentPlayer = computer;
        else currentPlayer = human;
    }

    /**
     * Getter method for the single gameboard.
     * @return the current board.
     */
    public Board getB() {
        return b;
    }

    /**
     * Getter method for the current player.
     * @return the current player - either human or computer.
     */
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Tests if a letter can be added. For simplicity, no move can be made
     * if 1) one tile has been placed and the move is not on either the row or
     * column that tile is placed, or 2) multiple tiles have been placed and
     * the attempted move does not follow the pattern already established - so
     * for example, if two tiles were placed on the same horizontal row, then
     * no move besides in that one row is processed.
     * @param x the x coordinate of the attempted move.
     * @param y the y coordinate of the attempted move.
     * @param c the character trying to be placed at the given location.
     * @return true if the letter can be placed there; false if not.
     */
    public boolean addLetter(int x, int y, char c) {
        boolean canPlace = false;
        //Can place anywhere with 0 placed tiles
        if (placed.size() == 0 && b.getBoard()[y][x].getC() == '\0') {
            canPlace = true;
        }
        //Can only place in row or column of the 1 placed tile
        else if (placed.size() == 1 && b.getBoard()[y][x].getC() == '\0') {
            for (Pair p: placed) {
                if ((int)p.getKey() == x) {
                    canPlace = true;
                }
                if ((int)p.getValue() == y) {
                    canPlace = true;
                }
            }
        }
        //Must follow already-established pattern with more than one letter
        else if (placed.size() >= 2 && b.getBoard()[y][x].getC() == '\0') {
            boolean isRight = false;
            if ((int)placed.get(0).getValue() == placed.get(1).getValue()) {
                isRight = true;
            }
            if (isRight) {
                for (Pair p: placed) {
                    if ((int)p.getValue() == y) {
                        canPlace = true;
                        break;
                    }
                }
            } else {
                for (Pair p: placed) {
                    if ((int)p.getKey() == x) {
                        canPlace = true;
                        break;
                    }
                }
            }
        }

        if (canPlace) {
            b.getBoard()[y][x].setC(c);
            placed.add(new Pair<>(x, y));
        }

        return canPlace;
    }

    /**
     * Method for changing the location on the board of a placed tile.
     * @param oldX the previous x coordinate of the tile.
     * @param oldY the previous y coordinate of the tile.
     * @param newX the new x coordinate of the tile, if valid.
     * @param newY the new y coordinate of the tile, if valid.
     * @return true if the tile can be moved; false if it can't.
     */
    public boolean canChange(int oldX, int oldY, int newX, int newY) {
        boolean validChange = false;
        char c = b.getBoard()[oldY][oldX].getC();
        b.getBoard()[oldY][oldX].pickUp();
        Pair p = null;
        for (Pair pair: placed) {
            if ((int)pair.getKey() == oldX && (int)pair.getValue() == oldY) {
                p = pair;
            }
        }
        if (p != null) placed.remove(p);
        if (addLetter(newX, newY, c)) validChange = true;
        else {
            b.getBoard()[oldY][oldX].setC(c);
            placed.add(new Pair<>(oldX, oldY));
        }
        return validChange;
    }

    /**
     * The main engine of determining if a move is valid. First makes sure
     * there's an unplayed tile on the board, then determine if the move is
     * going right or down. After that, do the corresponding cross-checks to
     * ensure the play is valid given the current placed tiles, and then test
     * if there are any stray letters on the board that aren't part of the
     * current word. Last is the adjacency test - if no moves have been made,
     * make sure the play is on top of the middle square; otherwise, make sure
     * the word touches at least one other played tile.
     * @return true if a given move is valid; false if it isn't.
     */
    public boolean isValid() {
        boolean works = false;
        int xStart = 0, yStart = 0;
        String s = "";
        boolean found = false;
        for (int i = 1; i < b.getDimension() + 1; i++) {
            for (int j = 1; j < b.getDimension() + 1; j++) {
                if (!b.getBoard()[i][j].isPlayed() && b.getBoard()[i][j].getC() != '\0') {
                    yStart = i;
                    xStart = j;
                    found = true;
                    break;
                }
                if (found) break;
            }
        }
        if (yStart != 0) { //xStart has to be non-zero if yStart is not zero
            boolean isRight = false;
            boolean isDown = false;
            //Since we've already searched from the top left, we only need to
            //search to the right and down for what the rest of the word is
            int j = 0;
            while (xStart + j < b.getDimension()) {
                if (!b.getBoard()[yStart][xStart + (++j)].isPlayed()
                        && b.getBoard()[yStart][xStart + j].getC() != '\0') {
                    isRight = true;
                    break;
                }
            }
            j = 0;
            if (!isRight) {
                while (yStart + j < b.getDimension()) {
                    if (!b.getBoard()[yStart + (++j)][xStart].isPlayed()
                            && b.getBoard()[yStart + j][xStart].getC() != '\0') {
                        isDown = true;
                        break;
                    }
                }
            }
            //Now that we know if it's right, down, or neither, we can make it
            //and check if it's even a word and if the cross-checks match up.
            //If it is valid, scan through the rest
            j = 1;
            int actualXStart = xStart;
            int actualYStart = yStart;
            boolean checksComplete = true;
            if (isRight) {
                //Get the string across
                s = Checker.verticalCheck(b, b.getBoard()[yStart][xStart].getC(), xStart, yStart);
                while (b.getBoard()[yStart][xStart - j].getC() != '\0'
                        && b.getBoard()[yStart][xStart - j].getC() != '0') {
//                    s = b.getBoard()[yStart][xStart - j].getC() + s;
                    actualXStart = xStart - j;
                    j++;
                }
                if (sol.getTrie().search(s.toLowerCase())) {
                    for (int i = 0; i < s.length(); i++) {
                        String smaller = Checker.acrossCheck(b, s.charAt(i),
                                actualXStart + i, actualYStart);
                        if (smaller.length() > 1 && !sol.getTrie().search(smaller.toLowerCase())) {
                            checksComplete = false;
                        }
                    }
                    if (checksComplete) works = true;
                } else checksComplete = false;
            } else if (isDown) {
                s = Checker.acrossCheck(b, b.getBoard()[yStart][xStart].getC(), xStart, yStart);
                while (b.getBoard()[yStart - j][xStart].getC() != '\0'
                        && b.getBoard()[yStart - j][xStart].getC() != '0') {
                    actualYStart = yStart - j;
                    j++;
                }
                if (sol.getTrie().search(s.toLowerCase())) {
                    for (int i = 0; i < s.length(); i++) {
                        String smaller = Checker.verticalCheck(b, s.charAt(i),
                                actualXStart, actualYStart + i);
                        if (smaller.length() > 1 && !sol.getTrie().search(smaller.toLowerCase())) {
                            checksComplete = false;
                        }
                    }
                    if (checksComplete) works = true;
                } else checksComplete = false;
            } else { //Only one tile has been placed down
                //Treat as horizontal, unless it's only hanging downward
                s = Checker.verticalCheck(b, b.getBoard()[yStart][xStart].getC(), xStart, yStart);
                if (s.length() > 1) {
                    isRight = true;
                    if (!sol.getTrie().search(s.toLowerCase())) {
                        checksComplete = false;
                    } else {
                        while (b.getBoard()[actualYStart][actualXStart - 1].getC() != '\0'
                                && b.getBoard()[actualYStart][actualXStart - 1].getC() != '0') {
                            actualXStart--;
                        }
                        for (int i = 0; i < s.length(); i++) {
                            String smaller = Checker.acrossCheck(b, s.charAt(i),
                                    actualXStart + i, actualYStart);
                            if (smaller.length() > 1 && !sol.getTrie().search(smaller.toLowerCase())) {
                                checksComplete = false;
                            }
                        }
                    }
                } else {
                    s = Checker.acrossCheck(b, b.getBoard()[yStart][xStart].getC(), xStart, yStart);
                    if (s.length() == 1) checksComplete = false; //Must be a free-floating letter
                    else {
                        isDown = true;
                        if (!sol.getTrie().search(s.toLowerCase())) {
                            checksComplete = false;
                        } else {
                            while (b.getBoard()[actualYStart - 1][actualXStart].getC() != '\0'
                                    && b.getBoard()[actualYStart - 1][actualXStart].getC() != '0') {
                                actualYStart--;
                            }
                            for (int i = 0; i < s.length(); i++) {
                                String smaller = Checker.verticalCheck(b, s.charAt(i),
                                        actualXStart, actualYStart + i);
                                if (smaller.length() > 1 && !sol.getTrie().search(smaller.toLowerCase())) {
                                    checksComplete = false;
                                }
                            }
                        }
                    }
                }
            }
            if (checksComplete) {
                boolean noStrayLetters = true;
                int tester = 0;
                if (isRight) {
                    tester = actualXStart + s.length();
                    for (int i = tester; i < b.getDimension(); i++) {
                        if (!b.getBoard()[actualYStart][i].isPlayed()
                                && b.getBoard()[actualYStart][i].getC() != '\0') {
                            noStrayLetters = false;
                            works = false;
                        }
                    }
                } else {
                    tester = actualYStart + s.length();
                    for (int i = tester; i < b.getDimension(); i++) {
                        if (!b.getBoard()[i][actualXStart].isPlayed()
                                && b.getBoard()[i][actualXStart].getC() != '\0') {
                            noStrayLetters = false;
                            works = false;
                        }
                    }
                }
                if (noStrayLetters) {
                    boolean finalChecks = true;
                    //if no previous moves, check if the middle tile is occupied.
                    //else, make sure it touches at least once
                    if (totalMoves == 0) {
                        if (b.getBoard()[b.getDimension()/2 + 1]
                                [b.getDimension()/2 + 1].getC() == '\0') {
                            finalChecks = false;
                        }
                    } else {
                        //make sure it touches
                        boolean isAdjacent = false;
                        if (isRight) {
                            for (int i = actualXStart; i < actualXStart + s.length(); i++) {
                                if (b.getBoard()[actualYStart + 1][i].isPlayed()
                                        || b.getBoard()[actualYStart - 1][i].isPlayed()
                                        || b.getBoard()[actualYStart][i - 1].isPlayed()
                                        || b.getBoard()[actualYStart][i + 1].isPlayed()) {
                                    isAdjacent = true;
                                    break;
                                }
                            }
                        } else {
                            for (int i = actualYStart; i < actualYStart + s.length(); i++) {
                                if (b.getBoard()[i][actualXStart + 1].isPlayed()
                                        || b.getBoard()[i][actualXStart - 1].isPlayed()
                                        || b.getBoard()[i + 1][actualXStart].isPlayed()
                                        || b.getBoard()[i - 1][actualXStart].isPlayed()) {
                                    isAdjacent = true;
                                    break;
                                }
                            }
                        }
                        if (!isAdjacent) {
                            finalChecks = false;
                        }
                    }
                    works = finalChecks;
                }
            }
            xStart = actualXStart;
            yStart = actualYStart;
            if (works) {
                moveScore = Checker.calculatePoints(b, s, xStart, yStart, isRight);
            }
        }
        return works;
    }

    /**
     * Once a move has been deemed legal and selected to be played, the move
     * is set in stone and finalized. Logistical issues, such as clearing the
     * list of placed-but-not-played letters, adding the score, etc. are also
     * dealt with to make the program ready for the next move.
     */
    public void finishMove() {
        for (int i = 1; i < b.getDimension() + 1; i++) {
            for (int j = 1; j < b.getDimension() + 1; j++) {
                if (!b.getBoard()[j][i].isPlayed()
                        && b.getBoard()[j][i].getC() != '\0') {
                    b.getBoard()[j][i].setPlayed();
                }
            }
        }
        placed.clear();
        human.addScore(moveScore);
        moveScore = 0;
        totalMoves++;
        human.refreshTiles(bag);
        changePlayer();
    }

    /**
     * Recalls all placed letters by iterating over the list of placed-but-not-
     * played letters and placing them back in the player's rack.
     */
    public void recall() {
        for (Pair pair: placed) {
            char c = b.getBoard()[(int)pair.getValue()]
                    [(int)pair.getKey()].getC();
            if (c < 'a') c = '*';
            human.addToTray(c);
            b.getBoard()[(int)pair.getValue()][(int)pair.getKey()].pickUp();
        }
        placed.clear();
    }

    /**
     * Places just one letter from the board back into the tray. Searches
     * through the list of placed tiles to make sure the coordinates match,
     * then uses a similar procedure as above to move the tile from the square
     * to the player's rack.
     * @param x the x-coordinate of the letter being picked up.
     * @param y the y-coordinate of the letter being picked up.
     */
    public void recallOne(int x, int y) {
        Pair toRemove = null;
        for (Pair pair: placed) {
            if (pair.getKey().equals(x) && pair.getValue().equals(y)) {
                char c = b.getBoard()[y][x].getC();
                if (c < 'a') {
                    c = '*';
                }
                human.addToTray(c);
                b.getBoard()[y][x].setC('\0');
                toRemove = pair;
            }
        }
        placed.remove(toRemove);
    }

    /**
     * Used for the game over condition. Tests if the computer has a valid
     * move anywhere; if it does, then the computer can move. It should be
     * a very rare case where the computer has no moves available.
     * @return true if the computer has a move; false if it doesn't.
     */
    public boolean canComputerMove() {
        return sol.solve(b, computer.trayString());
    }

    /**
     * Performs a computer move by finding the best word, placing it, and
     * completing the regular logistics of finishing up a move.
     */
    public void computerMove() {
        if (sol.solve(b, computer.trayString())) {
            computer.removeFromTray(sol.getHighScore(), b);
            b.placeWord(sol.getHighScore().getWord(),
                    sol.getHighScore().getxStart(),
                    sol.getHighScore().getyStart(),
                    sol.getHighScore().isRight());

            computer.addScore(sol.getHighScore().getPoints());
            totalMoves++;
        }
        computer.refreshTiles(bag);
        changePlayer();
    }

    /**
     * Tests if the game is over. If the bag is empty and one of the players
     * has no tiles left, the game is automatically over. Alternatively, if the
     * computer player has no moves left, the game is over.
     * @return
     */
    public boolean gameOver() {
        boolean isOver = false;
        if (bag.getLetters().size() == 0 && (human.trayString().length() == 0
                || computer.trayString().length() == 0)
                || !canComputerMove()) {
            isOver = true;
        }
        return isOver;
    }

    /**
     * Changes the player from human to computer and vice versa.
     */
    private void changePlayer() {
        if (currentPlayer.equals(human)) currentPlayer = computer;
        else currentPlayer = human;
    }

    /**
     * Getter method for the computer player.
     * @return the game's computer player.
     */
    public Player getComputer() {
        return computer;
    }

    /**
     * Getter method for the human player.
     * @return the game's human player.
     */
    public Player getHuman() {
        return human;
    }

    /**
     * Getter method for the game's solver object.
     * @return the game's solver.
     */
    public Solver getSol() {
        return sol;
    }

    /**
     * Getter method for the game's bag of tiles.
     * @return the game's bag of tiles.
     */
    public Bag getBag() {
        return bag;
    }
}