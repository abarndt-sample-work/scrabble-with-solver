/**
 * @author: Andrew Barndt
 * Class description: Reads a board and a tray to demonstrate the functionality
 * of the solver algorithm.
 */

package scrabble;

import java.util.Scanner;

public class Loader {

    /**
     * Entry point of the solver jar file. Takes a command line argument if
     * provided it and uses it as a dictionary.
     * @param args the command line arguments, if any.
     */
    public static void main(String[] args) {
        Loader l = new Loader();
        if (args.length > 0) {
            l.makeBoard(args[0], true);
        } else l.makeBoard("sowpods.txt", false);
    }

    /**
     * Makes as many boards as are provided, and tests the solver on them.
     * @param str the dictionary file the solver will use.
     * @param comLine if an argument was provided on the command line. If none
     *                were provided, then a default dictionary is used.
     */
    private void makeBoard(String str, boolean comLine) {
        Board b;
        String tray = "";
        int size;
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String num = sc.nextLine();
            size = Integer.parseInt(num);
            //Makes a board with the given size and tray
            b = new Board(size);
            for (int i = 0; i < size; i++) {
                b.rowOfBoard(sc.nextLine(), i + 1);
            }
            System.out.println("Input Board:");
            System.out.println(b.toString());
            if (sc.hasNextLine()) {
                tray = sc.nextLine();
                System.out.println("Tray: " + tray);
            }
            //Find the best word, then display the word, score, and updated
            //board
            Solver s = new Solver(str, comLine);
            s.solve(b, tray);
            b.placeWord(s.getHighScore().getWord(),
                    s.getHighScore().getxStart(), s.getHighScore().getyStart(),
                    s.getHighScore().isRight());
            System.out.println("Solution " + s.getHighScore().getWord() +
                    " has " + s.getHighScore().getPoints() + " points");
            System.out.println("Solution Board:");
            System.out.println(b.toString());
            System.out.println();
        }
    }
}
