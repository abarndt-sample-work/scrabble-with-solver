/**
 * @author: Andrew Barndt
 * Class description: Operates the bag of 100 tiles used in Scrabble.
 */

package scrabble;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;


public class Bag {
    private List<Character> letters = new ArrayList<>();

    /**
     * Constructs a new Bag object from the text file. Uses try-with-resources,
     * then skips the point value of a tile which is mapped out already in the
     * LetVal class. Shuffles the bag at the end.
     */
    public Bag () {
        try (InputStream in = getClass().getClassLoader().
                getResourceAsStream("scrabble_tiles.txt");
        Scanner scanner = new Scanner(new BufferedReader
                (new InputStreamReader(in)))) {
            while (scanner.hasNext()) {
                char c = scanner.next().charAt(0);
                int skip = scanner.nextInt();
                int appearances = scanner.nextInt();
                for (int i = 0; i < appearances; i++) {
                    addToBag(c);
                }
            }
        } catch (IOException ie) {
            System.out.println("Error loading the bag.");
        }
        shuffle();
    }

    /**
     * Draws a tile from the bag.
     * @return the character of the tile just drawn.
     */
    public Character draw() {
        if (letters.size() > 0) {
            return letters.remove(letters.size() - 1);
        }
        else return null;
    }

    /**
     * Adds a tile to the bag. Used in the constructor as well as for when an
     * exchange occurs.
     * @param c the character being added.
     */
    public void addToBag(char c) {
        letters.add(c);
    }

    /**
     * Getter method for the list of characters in the bag.
     * @return the list of characters in the bag.
     */
    public List<Character> getLetters() {
        return letters;
    }

    /**
     * Shuffles the list, as defined by the Collections shuffle method.
     */
    public void shuffle() {
        Collections.shuffle(letters);
    }
}