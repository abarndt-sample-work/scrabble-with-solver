/**
 * @author: Andrew Barndt
 * Class description: Holds a few regularly-used constants for the game
 * to access easily.
 */

package scrabble;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class ScrabbleConstants {

    //Store max number of letters a player can have, the font size on each
    //tile, and the size of the square as displayed by the GUI
    public static final int PLAYER_LETTERS = 7;
    public static final int LETTER_SIZE = 18;
    private static Rectangle2D screenSize = Screen.getPrimary().getBounds();
    private static double screenHeight = screenSize.getHeight();

    public static final double TILE_HEIGHT = screenHeight/22;

    //The below was typed by Matt and Sean Timm while I left my laptop open.
    //I consider it too hilarious to not keep in commented form.
    /*
    protected boolean loser = true;
    protected String rickRoll = "We're no strangers to love\n" +
            "You know the rules and so do I\n" +
            "A full commitment's what I'm thinking of\n" +
            "You wouldn't get this from any other guy\n" +
            "I just wanna tell you how I'm feeling\n" +
            "Gotta make you understand\n" +
            "Never gonna give you up\n" +
            "Never gonna let you down\n" +
            "Never gonna run around and desert you\n" +
            "Never gonna make you cry\n" +
            "Never gonna say goodbye\n" +
            "Never gonna tell a lie and hurt you\n" +
            "We've known each other for so long\n" +
            "Your heart's been aching but you're too shy to say it\n" +
            "Inside we both know what's been going on\n" +
            "We know the game and we're gonna play it\n" +
            "And if you ask me how I'm feeling\n" +
            "Don't tell me you're too blind to see\n" +
            "Never gonna give you up\n" +
            "Never gonna let you down\n" +
            "Never gonna run around and desert you\n" +
            "Never gonna make you cry\n" +
            "Never gonna say goodbye\n" +
            "Never gonna tell a…";
            */
}
