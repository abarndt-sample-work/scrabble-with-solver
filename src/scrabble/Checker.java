/**
 * @author: Andrew Barndt
 * Class description: a class with static methods to help calculate the
 * validity of moves given the tiles they intersect with, and to calculate the
 * Scrabble points of a given move.
 */

package scrabble;

public class Checker {

    /**
     * Generates the string of placing a tile with tiles already placed below
     * or above it. Can be used to determine if that string represents an
     * actual word.
     * @param b the current game board.
     * @param c the character being placed.
     * @param x the x-position of the tile placement.
     * @param y the y position of the tile placement
     * @return the string of tiles adjacent above and below.
     */
    public static String acrossCheck(Board b, char c, int x, int y) {
        String s = "" + c;
        int j = 0;
        while (b.getBoard()[y - (++j)][x].getC() != '\0'
                && b.getBoard()[y - j][x].getC() != '0') {
            s = b.getBoard()[y - j][x].getC() + s;
        }
        j = 0;
        while (b.getBoard()[y + (++j)][x].getC() != '\0'
                && b.getBoard()[y + j][x].getC() != '0') {
            s += b.getBoard()[y + j][x].getC();
        }
        return s;
    }

    /**
     * Generates the string of placing a tile with tiles already placed to the
     * left or to the right of it. Can be used to determine if that string
     * represents an actual word.
     * @param b the current game board.
     * @param c the character being placed.
     * @param x the x-position of the tile placement.
     * @param y the y position of the tile placement
     * @return the string of tiles on the left and right of the placed tile.
     */

    public static String verticalCheck(Board b, char c, int x, int y) {
        String s = "" + c;
        int j = 0;
        while (b.getBoard()[y][x - (++j)].getC() != '\0'
                && b.getBoard()[y][x - j].getC() != '0') {
            s = b.getBoard()[y][x - j].getC() + s;
        }
        j = 0;
        while (b.getBoard()[y][x + (++j)].getC() != '\0'
                && b.getBoard()[y][x + j].getC() != '0') {
            s += b.getBoard()[y][x + j].getC();
        }
        return s;
    }

    /**
     * Calculates the point value of a move. Documented in more detail below,
     * as well as in the ReadMe.
     * @param b the current board, before the move occurs.
     * @param word the word being played (including tiles that have already
     *             been played).
     * @param x the x-coordinate of the word's starting point.
     * @param y the y-coordinate of teh word's starting point.
     * @param toRight if the word is played horizontally
     * @return
     */
    public static int calculatePoints (Board b, String word, int x, int y, boolean toRight) {
        LetVal lv = new LetVal();
        int xCoor = x;
        int yCoor = y;
        int pointTotal = 0;
        int alreadyUsed = 0; //Add 1 when the word uses an already-placed tile
        if (toRight) {
            for (int i = 0; i < word.length(); i++) {
                if (b.getBoard()[y][xCoor].isPlayed()) {
                    alreadyUsed++;
                }
                xCoor++;
            }
            xCoor = x;
            int horizontalWordTotal = 0;
            int wordMultiple = 1;
            //For each letter, calculate the number of points that particular
            //tile scores, and save any total word multipliers.
            for (int i = 0; i < word.length(); i++) {
                horizontalWordTotal += lv.getLetterPoints().get(word.charAt(i))
                        * b.getBoard()[y][xCoor].getLetterBonus();
                wordMultiple *= b.getBoard()[y][xCoor].getWordBonus();
                //For each letter, calculate the points scored vertically too
                //if there's letters above/below
                if (!b.getBoard()[y][xCoor].isPlayed()) {
                    int vertScore = 0;
                    int vertMultiple = 1;
                    int j = 0;
                    int charCount = 1;
                    while (b.getBoard()[y - (++j)][xCoor].isPlayed()) {
                        charCount++;
                        vertScore += lv.getLetterPoints().get(b.getBoard()[y - j][xCoor].getC())
                                * b.getBoard()[y - j][xCoor].getLetterBonus();
                    }
                    j = 0;
                    while (b.getBoard()[y + (++j)][xCoor].isPlayed()) {
                        charCount++;
                        vertScore += lv.getLetterPoints().get(b.getBoard()[y + j][xCoor].getC())
                                * b.getBoard()[y + j][xCoor].getLetterBonus();
                    }
                    if (charCount > 1) {
                        vertMultiple = b.getBoard()[y][xCoor].getWordBonus();
                        vertScore += lv.getLetterPoints().get(word.charAt(i))
                                * b.getBoard()[y][xCoor].getLetterBonus();
                    }
                    vertScore *= vertMultiple;
                    pointTotal += vertScore;
                }
                xCoor++;
            }
            //Factor in multipliers for the horizontal score.
            horizontalWordTotal *= wordMultiple;
            pointTotal += horizontalWordTotal;
        } else {
            // Same procedure for the vertical words, except that the cross
            // checks are horizontal
            for (int i = 0; i < word.length(); i++) {
                if (b.getBoard()[yCoor][x].isPlayed()) {
                    alreadyUsed++;
                }
                yCoor++;
            }
            yCoor = y;
            int verticalWordTotal = 0;
            int wordMultiple = 1;
            for (int i = 0; i < word.length(); i++) {
                verticalWordTotal += lv.getLetterPoints().get(word.charAt(i))
                        * b.getBoard()[yCoor][x].getLetterBonus();
                wordMultiple *= b.getBoard()[yCoor][x].getWordBonus();
                if (!b.getBoard()[yCoor][x].isPlayed()) {
                    int horzScore = 0;
                    int horzMultiple = 1;
                    int j = 0;
                    int charCount = 1;
                    while (b.getBoard()[yCoor][x - (++j)].isPlayed()) {
                        charCount++;
                        horzScore += lv.getLetterPoints().get(b.getBoard()[yCoor][x - j].getC())
                                * b.getBoard()[yCoor][x - j].getLetterBonus();
                    }
                    j = 0;
                    while (b.getBoard()[yCoor][x + (++j)].isPlayed()) {
                        charCount++;
                        horzScore += lv.getLetterPoints().get(b.getBoard()[yCoor][x + j].getC())
                                * b.getBoard()[yCoor][x + j].getLetterBonus();
                    }
                    if (charCount > 1) {
                        horzMultiple = b.getBoard()[yCoor][x].getWordBonus();
                        horzScore += lv.getLetterPoints().get(word.charAt(i))
                                * b.getBoard()[yCoor][x].getLetterBonus();
                    }
                    horzScore *= horzMultiple;
                    pointTotal += horzScore;
                }
                yCoor++;
            }
            verticalWordTotal *= wordMultiple;
            pointTotal += verticalWordTotal;
        }

        //If the player used 7 of their own tiles, add the 50 point bonus
        if (word.length() - alreadyUsed == ScrabbleConstants.PLAYER_LETTERS) {
            pointTotal += 50;
        }
        return pointTotal;
    }
}