/**
 * @author: Andrew Barndt
 * Enum description: all the important states the game could be in that would
 * require an updating of the game in some way.
 */
package scrabble;

public enum State {
    RECALLING_ALL,
    RECALLING_ONE,
    PASS,
    PLAYING,
    COMPUTER_TURN,
    HUMAN_TURN,
    EXCHANGING,
    PLACING_TILE,
    MOVING_TILE,
    HOLDING_PLACED,
    HOLDING_RACK,
    GAME_OVER
}
