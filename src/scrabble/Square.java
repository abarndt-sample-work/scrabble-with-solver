/**
 * @author: Andrew Barndt
 * Class description: A class that maintains the squares that make up the board
 * in a Scrabble game.
 */

package scrabble;

public class Square {
    private int wordBonus, letterBonus;
    private int x, y;
    private boolean isPlayed = false, anchorSquare = false;
    private char c;

    /**
     * Square constructor for a square that has not been played. Sent the
     * information from the Board class as it constructs its own object
     * @param wordMult a multiplier for the entire word (double word, for
     *                 example).
     * @param letterMult (a multiplier for the individual letter (triple
     *                   letter, for example)
     * @param xCoor the x-coordinate within the board of the square.
     * @param yCoor the y-coordinate within the board of the square
     */
    public Square(char wordMult, char letterMult, int xCoor, int yCoor) {
        if (wordMult == '.') wordBonus = 1;
        else wordBonus = Integer.parseInt("" + wordMult);

        if (letterMult == '.') letterBonus = 1;
        else letterBonus = Integer.parseInt("" + letterMult);

        x = xCoor;
        y = yCoor;
    }

    /**
     * Constructing a square for an already-made square that just contains
     * a letter.
     * @param letter the letter, as converted from a string by a Scanner
     * @param xCoor the x-coordinate within the board of the square.
     * @param yCoor the y-coordinate within the board of the square
     */
    public Square(char letter, int xCoor, int yCoor) {
        c = letter;
        letterBonus = 1;
        wordBonus = 1;

        x = xCoor;
        y = yCoor;
    }

    /**
     * Getter method for the isPlayed field.
     * @return true if the square has been played; false if not.
     */
    public boolean isPlayed() {
        return isPlayed;
    }

    /**
     * Sets the square to the played state, which makes it not an anchor square
     * and makes the letter bonus automatically 1.
     */
    public void setPlayed() {
        isPlayed = true;
        anchorSquare = false;
        letterBonus = 1;
    }

    /**
     * Setter method for the anchorSquare boolean field.
     * @param b the true/false setting we want the anchorSquare field to have.
     */
    public void setAnchorSquare(boolean b) {
        anchorSquare = b;
    }

    /**
     * Getter method for the anchorSquare field.
     * @return true if the square currently is an anchor; false if not.
     */
    public boolean isAnchorSquare() {
        return anchorSquare;
    }

    /**
     * Getter method for the square's word multiplier.
     * @return the word multiplier on that square, if one exists.
     */
    public int getWordBonus() {
        if (isPlayed) return 1;
        return wordBonus;
    }

    /**
     * Getter method for the square's letter multiplier.
     * @return the letter multiplier on that square, if one exists.
     */
    public int getLetterBonus() {
        if (isPlayed) return 1;
        return letterBonus;
    }

    /**
     * Getter method for the character on the square.
     * @return the character on the square ('\0' if no character exists).
     */
    public char getC() {
        return c;
    }

    /**
     * Setter method for the character of the square.
     * @param character the new character on the square.
     */
    public void setC(char character) {
        c = character;
    }

    /**
     * Picks up a letter from the square, if the square hasn't been played
     * yet.
     */
    public void pickUp() {
        if (!isPlayed) c = '\0';
    }

    /**
     * Provides a string representation of the board.
     * @return the present multipliers, if any, or the played character.
     */
    public String toString() {
        String s = "";
        if (c != '\0') {
            s += " " + c;
        } else {
            if (wordBonus == 1) s += ".";
            else s += wordBonus;

            if (letterBonus == 1) s += ".";
            else s += letterBonus;
        }
        return s;
    }

    /**
     * Getter method for the x-coordinate of the square.
     * @return the x-coordinate of the square.
     */
    public int getX() {
        return x;
    }

    /**
     * Getter method for the y-coordinate of the square.
     * @return the y-coordinate of the square.
     */
    public int getY() {
        return y;
    }
}