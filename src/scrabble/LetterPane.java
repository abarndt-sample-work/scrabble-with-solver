/**
 * @author: Andrew Barndt
 * Class description: an extension of the BorderPane class that provides
 * extra functionality which is very useful to making the GUI.
 */

package scrabble;

import javafx.scene.layout.BorderPane;

public class LetterPane extends BorderPane {
    private boolean played = false;
    private boolean onBoard = false;
    private int x, y;

    /**
     * Getter method for the boolean played.
     * @return true if the pane represents a played tile; false if not.
     */
    public boolean isPlayed() {
        return played;
    }

    /**
     * Sets the played (cannot be removed) boolean to true; only called when
     * the play has been made final.
     */
    public void setPlayed() {
        this.played = true;
    }

    /**
     * Getter method for asking if the tile represented by the pane is on the
     * board (played or waiting to be played).
     * @return if the pane is on the board.
     */
    public boolean isOnBoard() {
        return onBoard;
    }

    /**
     * Setter method for indicating if the pane is on the board or not.
     * @param onBoard if the board is on the board.
     */
    public void setOnBoard(boolean onBoard) {
        this.onBoard = onBoard;
    }

    /**
     * Getter method for the x index of the pane.
     * @return the x index within the board of the pane.
     */
    public int getX() {
        return x;
    }

    /**
     * Getter method for the y-index of the pane.
     * @return the y-index within the board of the pane.
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the coordinates of the pane, once placed onto the board.
     * @param xCoor the x coordinate on the board the pane is on.
     * @param yCoor the y coordinate on the board the pane is on.
     */
    public void setCoors(int xCoor, int yCoor) {
        x = xCoor;
        y = yCoor;
    }
}