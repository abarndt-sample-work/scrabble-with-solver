/**
 * @author: Andrew Barndt
 * Class description: Provides a map of the point scores (or letter values,
 * hence the name LetVal) for each letter.
 */

package scrabble;

import java.util.HashMap;
import java.util.Map;

public class LetVal {
    private HashMap<Character, Integer> letterPoints = new HashMap<>();

    /**
     * Constructs the map with the point values of each letter, with capitals
     * representing blanks and scoring 0 points.
     */
    public LetVal() {
        letterPoints.put('a', 1);
        letterPoints.put('b', 3);
        letterPoints.put('c', 3);
        letterPoints.put('d', 2);
        letterPoints.put('e', 1);
        letterPoints.put('f', 4);
        letterPoints.put('g', 2);
        letterPoints.put('h', 4);
        letterPoints.put('i', 1);
        letterPoints.put('j', 8);
        letterPoints.put('k', 5);
        letterPoints.put('l', 1);
        letterPoints.put('m', 3);
        letterPoints.put('n', 1);
        letterPoints.put('o', 1);
        letterPoints.put('p', 3);
        letterPoints.put('q', 10);
        letterPoints.put('r', 1);
        letterPoints.put('s', 1);
        letterPoints.put('t', 1);
        letterPoints.put('u', 1);
        letterPoints.put('v', 4);
        letterPoints.put('w', 4);
        letterPoints.put('x', 8);
        letterPoints.put('y', 4);
        letterPoints.put('z', 10);

        //Make all capital letters, which will be represented as blanks,
        //worth zero points
        for (char i = 'A'; i <= 'Z'; i++) {
            letterPoints.put(i, 0);
        }
    }

    /**
     * Getter method for the map.
     * @return the map with the point values of each letter
     */
    public Map<Character, Integer> getLetterPoints() {
        return letterPoints;
    }
}
