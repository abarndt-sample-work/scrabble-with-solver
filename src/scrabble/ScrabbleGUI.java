/**
 * @author: Andrew Barndt
 * Class description: the graphic display for a game of Scrabble.
 */

package scrabble;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Optional;

import static scrabble.ScrabbleConstants.*;

public class ScrabbleGUI extends Application {

    //Many of these are accessed in multiple methods, so they're global
    private LetterPane hold = new LetterPane();
    private int clickedX = -1;
    private int clickedY = -1;
    private int selectedTrayIndex = -1;
    private State state;
    private ArrayList<LetterPane> notYetPlayed = new ArrayList<>();
    private FlowPane tray = new FlowPane();
    private GridPane gp = new GridPane();
    private DialogPane helpCenter = new DialogPane();
    private GameControl gc = new GameControl();
    private Button pass = new Button("Pass");
    private Button play = new Button("Play");
    private Button exchange = new Button ("Exchange");
    private Button recall = new Button("Recall");
    private Text l1 = new Text("Human score: 0");
    private Text l2 = new Text("Computer score: 0");
    private LetVal lv = new LetVal();
    private String list = "";
    private int passCount = 0;

    /**
     * Entry point of the game. Simply calls the launch method to display the
     * game.
     * @param args command line arguments, if any.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * The overriden start method that launches the GUI display. Uses a
     * GridPane to display the board, which is then put in an HBox with a VBox
     * to its left which displays things like the score, buttons, and tray.
     * Uses event handlers on the board, tray, buttons, and letters to maintain
     * an accurate game state and call the updateGame method accordingly.
     * @param s the overall stage for the game.
     */
    public void start(Stage s) {
        //Randomly determine who goes first
        if (gc.getCurrentPlayer().equals(gc.getHuman())) {
            state = State.HUMAN_TURN;
        } else state = State.COMPUTER_TURN;
        hold = null;

        //Initialize the board with StackPanes of either empty squares or with
        //multipliers
        for (int i = 1; i < gc.getB().getDimension() + 1; i++) {
            for (int j = 1; j < gc.getB().getDimension() + 1; j++) {
                StackPane sp = new StackPane();
                BorderPane fp = new BorderPane();
                fp.setMinHeight(TILE_HEIGHT);
                fp.setMinWidth(TILE_HEIGHT);
                fp.setMaxHeight(TILE_HEIGHT);
                fp.setMaxWidth(TILE_HEIGHT);
                if (gc.getB().getBoard()[j][i].getLetterBonus() == 2) {
                    fp.setBackground(new Background(new BackgroundFill(
                            Color.STEELBLUE, CornerRadii.EMPTY,
                            Insets.EMPTY)));
                    Text t = new Text("DL");
                    t.setFont(Font.font("Helvetica", FontWeight.BOLD, 14));
                    fp.setCenter(t);
                } else if (gc.getB().getBoard()[j][i].getLetterBonus() == 3) {
                    fp.setBackground(new Background(new BackgroundFill(
                            Color.LIMEGREEN, CornerRadii.EMPTY,
                            Insets.EMPTY)));
                    Text t = new Text("TL");
                    t.setFont(Font.font("Helvetica", FontWeight.BOLD, 14));
                    fp.setCenter(t);
                } else if (gc.getB().getBoard()[j][i].getWordBonus() == 2) {
                    fp.setBackground(new Background(new BackgroundFill(
                            Color.CRIMSON, CornerRadii.EMPTY, Insets.EMPTY)));
                    Text t = new Text("DW");
                    t.setFont(Font.font("Helvetica", FontWeight.BOLD, 14));
                    fp.setCenter(t);
                } else if (gc.getB().getBoard()[j][i].getWordBonus() == 3) {
                    fp.setBackground(new Background(new BackgroundFill(
                            Color.DARKORANGE, CornerRadii.EMPTY,
                            Insets.EMPTY)));
                    Text t = new Text("TW");
                    t.setFont(Font.font("Helvetica", FontWeight.BOLD, 14));
                    fp.setCenter(t);
                } else {
                    fp.setBackground(new Background(new BackgroundFill(
                            Color.BEIGE, CornerRadii.EMPTY, Insets.EMPTY)));
                }
                sp.getChildren().add(fp);
                sp.setMouseTransparent(true);
                gp.add(sp, i, j);
            }
        }
        gp.setGridLinesVisible(true);

        //Make the player's tray displayed
        for (int i = 0; i < ScrabbleConstants.PLAYER_LETTERS; i++) {
            LetterPane lp = new LetterPane();
            makeLetterPane(lp, i);
            tray.getChildren().add(lp);
        }

        //Allow the tray to have a single letter be recalled to the tray if
        //a letter on the board is being held and teh tray is clicked
        tray.setOnMouseClicked(event -> {
            if (state != State.HOLDING_RACK) {
                if (state == State.HOLDING_PLACED
                        || state == State.MOVING_TILE) {
                    state = State.RECALLING_ONE;
                    updateGame();
                } else {
                    hold = null;
                    selectedTrayIndex = -1;
                }
            }
        });

        //When the board is clicked, add a letter to or move a letter
        //around the board, if a letter is currently selected
        gp.setOnMouseClicked(event -> {
            if (state != State.COMPUTER_TURN) {
                clickedX = getXIndex(event);
                clickedY = getYIndex(event);
                if (hold != null && !hold.isOnBoard()) {
                    state = State.PLACING_TILE;
                    updateGame();
                } else if (hold != null && hold.isOnBoard()) {
                    if (state == State.HOLDING_PLACED) {
                        state = State.MOVING_TILE;
                    }
                    updateGame();
                }
            }
        });

        //Set event handlers for each button
        recall.setDisable(true);
        recall.setOnMouseClicked(event -> {
            state = State.RECALLING_ALL;
            updateGame();
        });

        //Remove all tiles from the board prior to exchanging, then open a
        //dialog so the user can enter what they want to exchange
        exchange.setOnMouseClicked(event -> {
            hold = null;
            gc.recall();
            for (LetterPane lp: notYetPlayed) {
                gp.getChildren().remove(lp);
                tray.getChildren().add(lp);
                lp.setOnBoard(false);
            }
            notYetPlayed.clear();
            TextInputDialog input = new TextInputDialog();
            input.setTitle("Exchange");
            input.setHeaderText("Type the letters, without spaces," +
                    "you'd want to exchange");

            //Only if the user enters actual letters is anything exchanged
            Optional<String> chars = input.showAndWait();
            if (chars.isPresent()) {
                for (int i = 0; i < input.getResult().length(); i++) {
                    if ((input.getResult().charAt(i) >= 'A'
                            && input.getResult().charAt(i) <= 'Z')
                            || ((input.getResult().charAt(i) >= 'a'
                            && input.getResult().charAt(i) <= 'z'))) {
                        list += input.getResult().charAt(i);
                    }
                }
                state = State.EXCHANGING;
                updateGame();
            }
        });
        play.setDisable(true);
        play.setOnMouseClicked(event -> {
            state = State.PLAYING;
            updateGame();
        });
        pass.setOnMouseClicked(event ->  {
            state = State.PASS;
            updateGame();
        });

        //All the formatting stuff for the GUI to look somewhat nice
        tray.setPadding(new Insets (50,5,5,5));
        tray.setHgap(4);
        tray.setBorder(new Border(new BorderStroke(Color.DIMGRAY,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
                BorderWidths.DEFAULT)));
        HBox buttons = new HBox(pass, recall, exchange, play);
        buttons.setSpacing(4);
        buttons.setPadding(new Insets(50, 5, 5, 5));
        helpCenter.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
                BorderWidths.DEFAULT)));
        helpCenter.setBackground(new Background(new BackgroundFill(
                Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        helpCenter.setHeaderText("Message Center");
        FlowPane humanScore = new FlowPane();
        FlowPane computerScore = new FlowPane();
        l1.setFont(Font.font("Helvetica", 16));
        l2.setFont(Font.font("Helvetica", 16));
        humanScore.setPadding(new Insets(10,5,10,5));
        computerScore.setPadding(new Insets(10,5,10,5));
        humanScore.getChildren().add(l1);
        computerScore.getChildren().add(l2);
        VBox scoreDisplay = new VBox(computerScore, humanScore);
        VBox leftSide = new VBox(scoreDisplay, helpCenter, buttons, tray);
        HBox overall = new HBox(leftSide, gp);
        Scene scene = new Scene(overall);
        s.setScene(scene);
        s.getIcons().add(new Image(getClass().getClassLoader().
                getResourceAsStream("icon.png")));
        s.setTitle("Scrabble");
        s.show();
        if (state == State.HUMAN_TURN) {
            helpCenter.setContentText("Human starts the game.");
        }
        updateGame();
    }

    /**
     * Updates the game depending on the current game's state. This method is
     * basically one giant switch statement of the current game state.
     */
    private void updateGame() {
        switch (state) {
            case HUMAN_TURN:
                //Sets everything equal to what is needed for the start of a
                //regular human move
                pass.setDisable(false);
                exchange.setDisable(false);
                if (gc.getHuman().trayString().length()
                        != ScrabbleConstants.PLAYER_LETTERS) {
                    exchange.setDisable(true);
                    recall.setDisable(false);
                }
                break;
            case COMPUTER_TURN:
                //Has the computer make a move.
                computerPlay();
                break;
            case PLAYING:
                //Set in stone what is currently on the board, and display the
                //new tiles the player got
                passCount = 0;
                int j = gc.getHuman().getTray().size();
                gc.finishMove();
                for (int i = j; i < gc.getHuman().getTray().size(); i++) {
                    LetterPane lp = new LetterPane();
                    makeLetterPane(lp, i);
                    tray.getChildren().add(lp);
                }
                for (LetterPane lp: notYetPlayed) {
                    lp.setPlayed();
                }
                notYetPlayed.clear();
                l1.setText("Human score: " + gc.getHuman().getScore());
                gameOverCheck();
                if (state != State.GAME_OVER) computerPlay();
                break;
            case MOVING_TILE:
                //Remove and re-add at the selected location a letter that
                //was previously on the board
                if ((clickedX != hold.getX()
                        || clickedY != hold.getY())
                        && gc.canChange(hold.getX(), hold.getY(), clickedX,
                        clickedY)) {
                    gp.getChildren().remove(hold);
                    gp.add(hold, clickedX, clickedY);
                    hold.setCoors(clickedX, clickedY);
                    if (gc.isValid()) play.setDisable(false);
                    else play.setDisable(true);
                    hold = null;
                    state = State.HUMAN_TURN;
                }
                clickedX = -1;
                clickedY = -1;
                break;
            case PLACING_TILE:
                //Move a tile from the tray to the board. If it's a blank,
                //prompt the user for the letter they want it to represent,
                //and make sure the input is a valid letter
                char c = gc.getHuman().trayString().
                        charAt(selectedTrayIndex);
                if (c == '*') {
                    TextInputDialog blank = new TextInputDialog();
                    blank.setTitle("Letter Selection");
                    blank.setHeaderText("Choose a letter for the blank tile.");
                    blank.setContentText("Type one letter here: ");
                    Optional<String> result = blank.showAndWait();
                    if (result.isPresent() && result.get().length() == 1
                            && ((result.get().charAt(0) >= 'A'
                            && result.get().charAt(0) <= 'Z')
                            || ((result.get().charAt(0) >= 'a'
                            && result.get().charAt(0) <= 'z')))) {
                        c = result.get().toUpperCase().charAt(0);
                        Text t = new Text(getLetterContent(c));
                        t.setFont(Font.font("Tahoma", LETTER_SIZE));
                        hold.setCenter(t);
                    } else {
                        helpCenter.setContentText("Not a valid blank.");
                        hold = null;
                        break;
                    }
                }
                //Only add if the letter can actually be added there
                if (gc.addLetter(clickedX, clickedY, c)) {
                    helpCenter.setContentText("");
                    tray.getChildren().remove(selectedTrayIndex);
                    gc.getHuman().removeFromTray(selectedTrayIndex);
                    gp.add(hold, clickedX, clickedY);
                    hold.setOnBoard(true);
                    hold.setCoors(clickedX, clickedY);
                    hold.relocate(clickedX * TILE_HEIGHT,
                            clickedY * TILE_HEIGHT);
                    hold.toFront();
                    notYetPlayed.add(hold);
                    recall.setDisable(false);
                }
                if (gc.isValid()) play.setDisable(false);
                else play.setDisable(true);
                hold = null;
                clickedY = -1;
                clickedX = -1;
                state = State.HUMAN_TURN;
                break;
            case PASS:
                //Reset the board and tray for the computer player's turn
                helpCenter.setContentText("");
                for (LetterPane lp: notYetPlayed) {
                    gp.getChildren().remove(lp);
                    tray.getChildren().add(lp);
                    lp.setOnBoard(false);
                    if (gc.getB().getBoard()[lp.getY()][lp.getX()].getC()
                            < 'a') {
                        lp.setCenter(null);
                    }
                }
                gc.recall();
                notYetPlayed.clear();
                hold = null;
                play.setDisable(true);
                recall.setDisable(true);
                passCount++;
                gameOverCheck();
                if (state != State.GAME_OVER) computerPlay();
                break;
            case RECALLING_ALL:
                //Move all unplayed tiles and panes to the tray
                for (LetterPane lp: notYetPlayed) {
                    gp.getChildren().remove(lp);
                    tray.getChildren().add(lp);
                    lp.setOnBoard(false);
                    if (gc.getB().getBoard()[lp.getY()][lp.getX()].getC()
                            < 'a') {
                        lp.setCenter(null);
                    }
                    lp.setCoors(0,0);
                }
                gc.recall();
                notYetPlayed.clear();
                hold = null;
                play.setDisable(true);
                recall.setDisable(true);
                state = State.HUMAN_TURN;
                break;
            case RECALLING_ONE:
                //Move the selected tile that's on the board to the tray
                if (gc.getB().getBoard()[hold.getY()][hold.getX()].getC()
                        < 'a') {
                    hold.setCenter(null);
                }
                gc.recallOne(hold.getX(), hold.getY());
                gp.getChildren().remove(hold);
                tray.getChildren().add(hold);
                hold.setOnBoard(false);
                hold.setCoors(0,0);
                notYetPlayed.remove(hold);
                if (notYetPlayed.size() == 0) recall.setDisable(true);
                hold = null;
                if (gc.isValid()) play.setDisable(false);
                else play.setDisable(true);
                state = State.HUMAN_TURN;
                break;
            case EXCHANGING:
                //Make sure the list of exchanged tiles is not larger than the
                //bag holds
                ArrayList<Integer> exchangeIndexes = new ArrayList<>();
                ArrayList<Character> toExchange = new ArrayList<>();
                if (list.length() <= gc.getBag().getLetters().size()) {
                    for (int i = 0; i < list.length(); i++) {
                        if (gc.getHuman().getTray().contains(list.charAt(i))) {
                            int index = gc.getHuman().getTray().
                                    indexOf(list.charAt(i));
                            exchangeIndexes.add(index);
                            toExchange.add(gc.getHuman().getTray().remove(index));
                        }
                    }
                } else {
                    helpCenter.setContentText("The bag does not hold " +
                            "enough tiles to exchange that many.");
                }
                //Draw tiles to tray before adding exchanged tiles back into
                //the bag; otherwise we'd just redraw the same ones we just put
                //in
                gc.getHuman().refreshTiles(gc.getBag());
                for (Character adder: toExchange) {
                    gc.getBag().addToBag(adder);
                }
                gc.getBag().shuffle();
                for (Integer in: exchangeIndexes) {
                    tray.getChildren().remove((int)in);
                }
                for (int i = gc.getHuman().getTray().size()
                        - toExchange.size(); i < PLAYER_LETTERS; i++) {
                    LetterPane lp = new LetterPane();
                    makeLetterPane(lp, i);
                    tray.getChildren().add(lp);
                }
                exchangeIndexes.clear(); toExchange.clear();
                gameOverCheck();
                if (list.length() > 0 && state != State.GAME_OVER) {
                    computerPlay();
                }
                break;
        }
    }

    /**
     * Plays a computer play. Calls the method to play the best move, then
     * updates the display with the appropriate letters in the appropriate
     * panes to show the user what the computer just did. Finally, the final
     * logistics of completing a move are finished.
     */
    private void computerPlay() {
        helpCenter.setContentText("");
        pass.setDisable(true);
        play.setDisable(true);
        exchange.setDisable(true);
        recall.setDisable(true);
        gc.computerMove();
        if (gc.getSol().getHighScore().getxStart() != -1) {
            boolean isRight =
                    gc.getSol().getHighScore().isRight();
            int x = gc.getSol().getHighScore().getxStart();
            int y = gc.getSol().getHighScore().getyStart();
            String s = gc.getSol().getHighScore().getWord();
            if (isRight) {
                for (int i = x; i < x + s.length(); i++) {
                    LetterPane lp = new LetterPane();
                    lp.setMinHeight(TILE_HEIGHT);
                    lp.setBackground(new Background(new
                            BackgroundFill(Color.BURLYWOOD,
                            CornerRadii.EMPTY, Insets.EMPTY)));
                    Text t = new Text();
                    if (gc.getB().getBoard()[y][i].getC() < 'a') {
                        t.setFont(Font.font("Tahoma", LETTER_SIZE));
                        t.setText(getLetterContent(gc.getB().
                                getBoard()[y][i].getC()));
                    } else { //Convert lowercase char to uppercase
                        String tile = getLetterContent
                                (gc.getB().getBoard()[y][i].getC());
                        t.setFont(Font.font("Tahoma", LETTER_SIZE));
                        t.setText(tile);
                    }
                    lp.setCenter(t);
                    lp.setPlayed(); lp.setOnBoard(true);
                    gp.add(lp, i, y);
                    lp.toFront();
                }
            } else {
                for (int i = y; i < y + s.length(); i++) {
                    LetterPane lp = new LetterPane();
                    lp.setMinHeight(TILE_HEIGHT);
                    lp.setBackground(new Background(new
                            BackgroundFill(Color.BURLYWOOD,
                            CornerRadii.EMPTY, Insets.EMPTY)));
                    Text t = new Text();
                    if (gc.getB().getBoard()[i][x].getC() < 'a') {
                        t.setFont(Font.font("Tahoma", LETTER_SIZE));
                        t.setText(getLetterContent(gc.getB().
                                getBoard()[i][x].getC()));
                    } else { //Convert lowercase char to uppercase
                        String tile = getLetterContent
                                (gc.getB().getBoard()[i][x].getC());
                        t.setFont(Font.font("Tahoma", LETTER_SIZE));
                        t.setText(tile);
                    }
                    lp.setCenter(t);
                    lp.setPlayed(); lp.setOnBoard(true);
                    gp.add(lp, x, i);
                    lp.toFront();
                }
            }
        } else {
            helpCenter.setContentText("Computer couldn't move.");
        }
        l2.setText("Computer score: " + gc.getComputer().getScore());
        state = State.HUMAN_TURN;
        gameOverCheck();
        if (state != State.GAME_OVER) {
            exchange.setDisable(false);
            pass.setDisable(false);
        }
    }

    /**
     * Checks if the game is over. It's over if either the game logic says it
     * is, or if the computer cannot move and the current user has hit the pass
     * button (because they can't find a move) more than once instead of
     * exchanging or playing. If the game is over, then points are deducted as
     * necessary from players with letters still unplayed.
     */
    private void gameOverCheck() {
        if (gc.gameOver() || (!gc.canComputerMove() && passCount > 1)) {
            state = State.GAME_OVER;
            play.setDisable(true);
            exchange.setDisable(true);
            recall.setDisable(true);
            pass.setDisable(true);
        }
        if (state == State.GAME_OVER) {
            int toDeduct = 0;
            for (int i = 0; i < gc.getHuman().trayString().length(); i++) {
                toDeduct -= lv.getLetterPoints().get
                        (gc.getHuman().trayString().charAt(i));
            }
            gc.getHuman().addScore(toDeduct);
            l1.setText("Human score: " + gc.getHuman().getScore());
            toDeduct = 0;
            for (int i = 0; i < gc.getComputer().trayString().length(); i++) {
                toDeduct -= lv.getLetterPoints().get
                        (gc.getComputer().trayString().charAt(i));
            }
            gc.getComputer().addScore(toDeduct);
            l2.setText("Computer score: " + gc.getComputer().getScore());
            helpCenter.setContentText("Game over.");
        }
    }

    /**
     * Edits a letterPane object, being added to the player's tray, to the
     * appropriate letter and point value, if they exist. Also sets the event
     * handlers to know what to do when the pane is clicked. This is not called
     * when a computer places a word and a letterPane has to be created.
     * @param lp the pane being edited.
     * @param i the index within the tray.
     */
    private void makeLetterPane(LetterPane lp, int i) {
        lp.setMaxHeight(TILE_HEIGHT);
        lp.setMaxWidth(TILE_HEIGHT);
        lp.setMinHeight(TILE_HEIGHT);
        lp.setMinWidth(TILE_HEIGHT);
        lp.setBackground(new Background(new BackgroundFill(Color.BURLYWOOD,
                CornerRadii.EMPTY, Insets.EMPTY)));
        lp.setPadding(new Insets(5,5,5,5));

        //If it's a blank, do nothing; otherwise, put the letter in the pane
        if (gc.getHuman().trayString().charAt(i) != '*') {
            Text t = new Text();
            t.setText(getLetterContent(gc.getHuman().trayString().charAt(i)));
            t.setFont(Font.font("Tahoma", LETTER_SIZE));
            lp.setCenter(t);
        }
        lp.setOnMouseClicked(event -> {
            //Can't do anything if the game is over
            if (state != State.GAME_OVER) {
                //Cases of holding nothing and click on an unplayed tile or a
                //rack tile
                if (hold == null && !lp.isPlayed()) {
                    hold = lp;
                    if (lp.isOnBoard()) {
                        state = State.HOLDING_PLACED;
                    }
                    else {
                        selectedTrayIndex = tray.getChildren().indexOf(lp);
                        state = State.HOLDING_RACK;
                        updateGame();
                    }
                    //Clicking on tile in the rack while holding a tile puts
                    //the held tile in the rack
                } else if (hold != null && !lp.isOnBoard()) {
                    if (state == State.HOLDING_PLACED
                            || state == State.MOVING_TILE) {
                        state = State.RECALLING_ONE;
                        updateGame();
                    } else {
                        hold = null;
                        selectedTrayIndex = -1;
                    }
                    //Cases of holding a tile and placing it on a tile on the
                    //board
                } else if (hold != null) {
                    state = State.HUMAN_TURN;
                    hold = null;
                }
            }
        });
    }

    /**
     * Gets the x index on the gridPane of a given click. Gets the click's
     * x coordinate, divides it by the size of a tile with one added to it,
     * adds 1 to the final result, and returns the value as a truncated int.
     * @param event the clicking event.
     * @return the x index on the GridPane.
     */
    private int getXIndex(MouseEvent event) {
        double x = event.getX();
        x /= (TILE_HEIGHT + 1);
        x += 1;
        return (int)x;
    }

    /**
     * Gets the y index on the gridPane of a given click. Gets the click's
     * y coordinate, divides it by the size of a tile with one added to it,
     * adds 1 to the final result, and returns the value as a truncated int.
     * @param event the clicking event.
     * @return the y index on the GridPane.
     */
    private int getYIndex(MouseEvent event) {
        double y = event.getY();
        y /= (TILE_HEIGHT + 1);
        y += 1;
        return (int)y;
    }

    /**
     * Creates the string to be placed on the tile to indicate the letter and
     * point value. Displays a letter and then, in subscript, displays the
     * point value of that letter. If the letter is represented by a lowercase
     * letter, it is represented by a capital letter on the tile with its
     * corresponding point value next to it. If the letter is represented by
     * a capital letter, it must have been a blank and is worth no points; in
     * this case the letter is just placed on the tile with no points put
     * anywhere.
     * @param c the letter being placed.
     * @return the string to be added to the tile.
     */
    private String getLetterContent(char c) {
        String s = "";
        switch(lv.getLetterPoints().get(c)) {
            case 1:
                if (c >= 'a') c -= ' ';
                s += c + "" + '\u2081';
                break;
            case 2:
                if (c >= 'a') c -= ' ';
                s += c + "" + '\u2082';
                break;
            case 3:
                if (c >= 'a') c -= ' ';
                s += c + "" + '\u2083';
                break;
            case 4:
                if (c >= 'a') c -= ' ';
                s += c + "" + '\u2084';
                break;
            case 5:
                if (c >= 'a') c -= ' ';
                s += c + "" + '\u2085';
                break;
            case 8:
                if (c >= 'a') c -= ' ';
                s += c + "" + '\u2088';
                break;
            case 10:
                if (c >= 'a') c -= ' ';
                s += c + "" + '\u2081' + '\u2080';
                break;
            default:
                s += c + "";
        }
        return s;
    }
}