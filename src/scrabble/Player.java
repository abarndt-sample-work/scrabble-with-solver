/**
 * @author: Andrew Barndt
 * Class description: performs the functions a player in Scrabble would have -
 * maintain a tray of tiles, a score, and have a name.
 */

package scrabble;

import java.util.LinkedList;

public class Player {
    private String name;
    private int score;
    private LinkedList<Character> tray = new LinkedList<>();

    /**
     * Constructor for a player. Gives them a name and a score of 0 to start.
     * @param s the name of the player (either "human" or "computer")
     */
    public Player(String s) {
        name = s;
        score = 0;
    }

    /**
     * Adds as many tiles as is necessary or, if the bag is empty enough, as
     * possible.
     * @param b the bag, controlled by the overall game logic
     */
    public void refreshTiles(Bag b) {
        while (b.getLetters().size() > 0 &&
                tray.size() < ScrabbleConstants.PLAYER_LETTERS) {
            tray.add(b.draw());
        }
    }

    /**
     * Adds points to the player's score.
     * @param points the number of points to be added.
     */
    public void addScore(int points) {
        score += points;
    }

    /**
     * Removes a tile from the player's tray at the specified index.
     * @param index the index (0-6) of the tile to be removed.
     */
    public void removeFromTray(int index) {
        tray.remove(index);
    }

    /**
     * Removes a word from the tray given the best word played. Used after a
     * computer play. Removes letters from the tray that were actively played,
     * ignoring letters that were already on the board.
     * @param bw the BestWord object created by the solver's move
     * @param b the board for the game.
     */
    public void removeFromTray(BestWord bw, Board b) {
        int counter = 0;
        if (bw.isRight()) {
            for (int i = bw.getxStart(); i < bw.getxStart()
                    + bw.getWord().length(); i++) {
                if (!b.getBoard()[bw.getyStart()][i].isPlayed()) {
                    if (bw.getWord().charAt(counter) >= 'A'
                            && bw.getWord().charAt(counter) <= 'Z') {
                        tray.remove((Character)'*');
                    } else {
                        tray.remove((Character)bw.getWord().charAt(counter));
                    }
                }
                counter++;
            }
        } else {
            for (int i = bw.getyStart(); i < bw.getyStart()
                    + bw.getWord().length(); i++) {
                if (!b.getBoard()[i][bw.getxStart()].isPlayed()) {
                    if (bw.getWord().charAt(counter) >= 'A'
                            && bw.getWord().charAt(counter) <= 'Z') {
                        tray.remove((Character)'*');
                    } else {
                        tray.remove((Character)bw.getWord().charAt(counter));
                    }
                }
                counter++;
            }
        }
    }

    /**
     * Adds a character to the player's tray.
     * @param c the character being added.
     */
    public void addToTray(char c) {
        tray.add(c);
    }

    /**
     * Returns a string of each letter in the tray concatenated together.
     * @return the letters in the tray in one string.
     */
    public String trayString() {
        String s = "";
        for (Character c: tray) {
            s += c;
        }
        return s;
    }

    /**
     * Getter method for the player's name.
     * @return The player's name - either "human" or "computer"
     */
    public String getName() {
        return name;
    }

    /**
     * Getter method for the player's score.
     * @return the player's score.
     */
    public int getScore() {
        return score;
    }

    /**
     * Getter method for the player's current tray of tiles
     * @return the list of characters the player has.
     */
    public LinkedList<Character> getTray() {
        return tray;
    }
}
