/**
 * @author: Andrew Barndt
 * Class description: Creates and maintains the board in the game.
 */

package scrabble;

public class Board {

    private int dimension;
    private Square[][] board;

    /**
     * Constructor for the board. Is fed an int representing the size of the
     * board, which is then added to by 2 to create a buffer around the edge
     * to worry less about going out of array bounds.
     * @param size the width and height of the square board.
     */
    public Board(int size) {
        dimension = size;
        board = new Square[dimension + 2][dimension + 2];
        for (int i = 0; i < dimension + 2; i++) {
            board[i][0] = new Square('0', 0 ,i);
            board[0][i] = new Square('0',  i, 0);
            board[i][dimension + 1] = new Square('0', dimension + 1, i);
            board[dimension + 1][i] = new Square('0', i, dimension + 1);
        }
    }

    /**
     * Adds one row of a board at a time. Called by a method in another, higher
     * order class several times to fill in a row.
     * @param s the string representation of the row.
     * @param row the index of the row being made.
     */
    public void rowOfBoard(String s, int row) {
        int counter = 1;
        for (int i = 0; i < s.length(); i++) {
            if (i % 3 == 0 && s.charAt(i) == ' ') {
                board[row][counter] = new Square(s.charAt(++i), counter, row);
                board[row][counter].setPlayed();
                counter++;
            } else {
                board[row][counter] = new Square(s.charAt(i), s.charAt(++i), counter++, row);
            }
            i++;
        }
    }

    /**
     * String representation of the board. Prints out each square's toString.
     * @return a string of the dimension x dimension board.
     */
    public String toString() {
        String s = "";
        for (int i = 1; i < dimension + 1; i++) {
            for (int j = 1; j < dimension + 1; j++) {
                s += board[i][j].toString();
                if (j != dimension) s += " ";
            }
            if (i != dimension) s += "\n";
        }
        return s;
    }

    /**
     * Places a word officially on the board.
     * @param word the full word being played.
     * @param x the x starting coordinate of the move.
     * @param y the y starting coordinate of the move.
     * @param toRight indicating if the word is played horizontal or vertical.
     */
    public void placeWord(String word, int x, int y, boolean toRight) {
        for (int i = 0; i < word.length(); i++) {
            if (toRight) {
                board[y][x + i].setC(word.charAt(i));
                board[y][x + i].setPlayed();
            } else {
                board[y + i][x].setC(word.charAt(i));
                board[y + i][x].setPlayed();
            }
        }
    }

    /**
     * Getter method for the squares of the board.
     * @return a 2D array of squares.
     */
    public Square[][] getBoard() {
        return board;
    }

    /**
     * Getter method for the size of the board.
     * @return the width/height of the board.
     */
    public int getDimension() {
        return dimension;
    }
}